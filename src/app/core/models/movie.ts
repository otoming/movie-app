import { genreType } from './genreType';

export class Movie {
	id: number;
	key: string;
	name: string;
	description: string;
	genres: genreType[];
	rate: string;
	length: string;
	img: string;
}
