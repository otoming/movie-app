import { Component } from '@angular/core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['../assets/scss/components/app.component.scss']
})
export class AppComponent { }
