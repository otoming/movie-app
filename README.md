# Movies Application

New movies collection application

### Application features
- **Movies List**
  - List Movies.
  - Search.
  - Filter.
  - Unit tests.
  - Clicking on a particular movie will redirect you to the selected &#39;movie detail&#39; page.
- **Movie detail**
  - Show selected movie detail information.
  - Navigate back to &#39;movie list&#39;.

### Technologies used
- [Angular 7](https://angular.io/)
- [TypeScript](https://www.typescriptlang.org/)
- [Rxjs](https://github.com/ReactiveX/rxjs)
- [Sass](http://sass-lang.com/)
- [Redux](http://redux.js.org/)

As this is standalone frontend app movie data is hardcoded in store's initial state

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`.

## Build

Run `npm build` to build the project.

## Running unit tests

Run `npm test` to run the unit tests

